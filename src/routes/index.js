'use strict'
import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer'
import { Home, MyLinks} from '@screens';


const Drawer = createDrawerNavigator();

function Routes(){
    return(
        <Drawer.Navigator 
            drawerContentOptions={{
                activeBackgroundColor:'#2ccbb9',
                activeTintColor: '#fff',
                marginTop: 16,
                labelStyle:{
                    fontSize:19,
                }
            }}
        
        >
            <Drawer.Screen
                options= {{
                    title: "Shorten Links"
                }}
                name= "Home"
                component= {Home}                
            />
            <Drawer.Screen
            options= {{
                title: "My Links"
            }}
                name="MyLinks"
                component={MyLinks}
            />
        </Drawer.Navigator>
    )
}
export default Routes;
'use strict'
import React from 'react';
import { View, Text } from 'react-native';
import { StatusBarPage } from '@screens';
import {Menu} from '@screens';

export default function MyLinks() {
    return (
        <View>

            <StatusBarPage
                barStyle="light-content"
                backgroundColor="#132742"
            />
            <Text>
                MyLinks Page
            </Text>
        </View>
    )
}
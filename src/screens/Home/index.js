'use strict'
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { StatusBarPage } from '@screens';
import { Menu } from '@screens';
import {
    ContainerLogo,
    Logo,
    ContainerContent,
    Title,
    SubTitle,
    ContainerImput,
    BoxIcon,
    Input,
    LinkImage,
    ButtonLink,
    ButtonLinkText
} from './styles';
import { GetUrl } from '@Api'

export default function Home() {

    const state = {
        nome: ''
    }
    const longLink = () => {   
        GetUrl(state.nome);
    }

    return (
        <LinearGradient
            colors={['#1ddbb9', '#132742']}
            style={{ flex: 1, justifyContent: 'center' }}
        >
            <StatusBarPage
                barStyle="light-content"
                backgroundColor="#1ddbb9"
            />
            <Menu />

            <ContainerLogo>
                <Logo source={require('../../assets/Logo.png')} />
            </ContainerLogo>

            <ContainerContent>
                <Title> Encurtador de link </Title>
                <SubTitle> Cole aqui seu Link </SubTitle>
                <ContainerImput>
                    <BoxIcon>

                        <LinkImage source={require('../../assets/link.png')} />
                    </BoxIcon>
                    <Input
                        onChangeText={text => state.nome = text}
                        placeholder='Cole seu link Aqui...'
                        placeholderTextColor="white"

                    />
                </ContainerImput>

                <ButtonLink onPress={() => longLink()}>
                    <ButtonLinkText> Gerar Link </ButtonLinkText>
                </ButtonLink>

            </ContainerContent>

        </LinearGradient>
    )
};

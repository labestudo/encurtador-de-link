'use strict'

import React from 'react';
import {ButtonMenu} from './styles';
import  { Image, Text, View, StyleSheet }  from 'react-native';
import { useNavigation } from '@react-navigation/native';


export default function Menu(){

    const navigation = useNavigation();

    return(
             <ButtonMenu onPress={ () => navigation.openDrawer()}>
                 <Text> Menu </Text>
             </ButtonMenu>
    )
}

const styles = StyleSheet.create({
    image: {
        width: 200,
        height: 220,
        color:'#fff'
      },
})